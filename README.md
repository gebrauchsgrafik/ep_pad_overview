# ep_pad_overview

> A plugin for Etherpad-lite that shows a list of all pads sorted by last edit date.

This plugin is a fork of [ep_pad-lister](https://github.com/ktt-ol/ep_pad-lister/). It works with the new promise-based API of etherpad-lite and has less dependencies. Otherwise the functionality is basically the same.

# Features

- List all pads, shows revision and last edit date
- Ignores pads without any changes
- Ignores private pads (= pads starting with `private_`)

# Screenshot

![Screenshot](./docs/screenshot.png)
