var eejs = require("ep_etherpad-lite/node/eejs");
var settings = require("ep_etherpad-lite/node/utils/Settings");
var padManager = require("ep_etherpad-lite/node/db/PadManager");
var PRIVATE_PAD_PREFIX =
  (settings.ep_pad_overview && settings.ep_pad_overview_private_pad_prefix) ||
  "private_";

exports.eejsBlock_indexWrapper = function(hook_name, args, cb) {
  var render_args = {
    PRIVATE_PAD_PREFIX: PRIVATE_PAD_PREFIX
  };
  args.content =
    args.content +
    eejs.require("ep_pad_overview/templates/linkToList.ejs", render_args);
  return cb();
};

exports.registerRoute = function(hook_name, args, cb) {
  args.app.get("/all-pads", function(req, res) {
    getDetailedPadList()
      .then(function(pads) {
        var render_args = {
          PRIVATE_PAD_PREFIX: PRIVATE_PAD_PREFIX,
          pads: pads
        };
        res.send(
          eejs.require("ep_pad_overview/templates/list.html", render_args)
        );
      })
      .catch(function(e) {
        console.error("Error building pad list!");
        console.debug(e);
        res.status(500);
      });
  });
};

async function getDetailedPadList() {
  var allPads = await padManager.listAllPads();
  var allPadNames = allPads.padIDs;
  var publicPadNames = allPadNames.filter(function(padName) {
    return padName.indexOf(PRIVATE_PAD_PREFIX) !== 0;
  });
  var padInfos = await Promise.all(
    publicPadNames.map(async function(padName) {
      // Get the pad and put it into the correct form
      var pad = await padManager.getPad(padName);
      // If this is an invalid pad or an empty one, discard it.
      if (!pad || pad.head === 0) {
        return;
      }
      var time = await pad.getLastEdit();
      return {
        name: padName,
        lastRevision: pad.head,
        lastEditTime: time
      };
    })
  );
  return padInfos
    .filter(function(padInfo) {
      return typeof padInfo !== "undefined";
    })
    .sort(function(a, b) {
      return b.lastEditTime - a.lastEditTime;
    })
    .map(function(pad) {
      return {
        name: pad.name,
        lastRevision: pad.lastRevision,
        lastEditTime: formatDate(pad.lastEditTime)
      };
    });
}

function formatDate(timestamp) {
  var dateLocale =
    (settings.ep_pad_overview && settings.ep_pad_overview_date_locale) ||
    "en-US";
  var dateStyle =
    (settings.ep_pad_overview && settings.ep_pad_overview_date_style) ||
    "short";
  return new Date(timestamp).toLocaleString(dateLocale, {
    dateStyle: dateStyle
  });
}
